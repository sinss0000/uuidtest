package none.com.uuidtest;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.UUID;

/**
 * Created by leo.chang on 18/12/2017.
 */

public class UUIDKit {
    private final static String KEY_UUID = "KEY_UUID";

    /**
     * 從SharedPreference取得一組UUID，如果不存在，產生一組新的，並且儲存到SharedPresences。
     * @param context
     * @return UUID string
     * */
    public static String getUUID(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("UUIDKit", Context.MODE_PRIVATE);
        String uuid = sharedPref.getString(KEY_UUID, "");
        if (uuid.isEmpty()) {
            uuid = UUID.randomUUID().toString();
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(KEY_UUID, uuid);
            editor.commit();
        }
        return uuid;
    }
}
